import 'package:get/get.dart';
import 'package:get_x_sqlite/src/products/controllers/product_form_controller.dart';

class ProductFormBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(ProductFormController());
  }
}