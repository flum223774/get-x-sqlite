import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_sqlite/src/products/controllers/product_controller.dart';
import 'package:get_x_sqlite/src/products/models/category_model.dart';
import 'package:get_x_sqlite/src/products/models/product_model.dart';
import 'package:get_x_sqlite/src/products/providers/product_provider.dart';

class ProductFormController extends GetxController {
  ProductController _productController = Get.find();
  RxList<CategoryModel> _categories = RxList([]);

  // GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  ProductProvider _productProvider = Get.find();

  // RxnString _productName = RxnString(null);
  // RxnString _productDesc = RxnString(null);
  // RxnString _productPic = RxnString(null);
  // RxnDouble _price = RxnDouble(null);
  // RxnInt _categoryId = RxnInt(null);
  RxBool isEditMode = RxBool(false);

  Rx<ProductModel> _product = Rx<ProductModel>(ProductModel.init());

  ProductModel get product => _product.value;

  set product(ProductModel productModel) {
    _product(productModel);
  }

  List<CategoryModel> get categories => _categories;

  // String? get productName => _productName.value;
  //
  // String? get productDesc => _productDesc.value;
  //
  // String? get productPic => _productPic.value;
  //
  // double? get price => _price.value;
  //
  // int? get categoryId => _categoryId.value;

  // set productName(String? val) => _productName.value = val;
  //
  // set productDesc(String? val) => _productDesc.value = val;
  //
  // set productPic(String? val) => _productPic.value = val;
  //
  // set price(double? val) => _price.value = val;
  //
  // set categoryId(int? val) => _categoryId.value = val;

  @override
  onInit() {
    super.onInit();
    var arguments = Get.arguments;
    print(arguments);
    if (arguments != null) {
      isEditMode(arguments['isEditMode']);
      _product(arguments['model'] as ProductModel);
      print(_product.value.id);
    }
    _getCategories();
  }

  Future<void> _getCategories() async {
    _categories(await _productProvider.getCategories());
  }

  Future<void> saveProduct() async {
    var isSave = false;
    if (isEditMode.value) {
      isSave = await _productProvider.updateProduct(product);
      if (isSave) {
        _productController.updateProduct(product);
      }
    } else {
      var productID = await _productProvider.addProduct(product);
      if (productID > 0) {
        var newProduct = await _productProvider.getProduct(productID);
        print('NewProduct ${newProduct?.id}');
        if (newProduct != null) {
          _productController.addProduct(newProduct);
        }
        isSave = true;
      }
    }
    print('add New $isSave');
    if (isSave) {
      Get.back();
    }
    return;
  }
}
