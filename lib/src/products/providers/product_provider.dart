import 'package:get_x_sqlite/core/utils/db_helper.dart';
import 'package:get_x_sqlite/src/products/models/category_model.dart';
import 'package:get_x_sqlite/src/products/models/product_model.dart';

class ProductProvider {
  Future<int> addProduct(ProductModel? model) async {
    await DB.init();
    if (model != null) {
      return await DB.insert(ProductModel.table, model);
    }
    return 0;
  }

  Future<bool> updateProduct(ProductModel? model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.update(ProductModel.table, model);
      print('UpdatedRaw $inserted');
      isSaved = inserted == 1 ? true : false;
    }
    return isSaved;
  }

  Future<List<ProductModel>> getProducts() async {
    await DB.init();
    List<Map<String, dynamic>> products = await DB.query(ProductModel.table);

    return products.map((item) => ProductModel.fromMap(item)).toList();
  }

  Future<ProductModel?> getProduct(int id) async {
    await DB.init();
    List<Map<String, dynamic>> products =
        await DB.getOne(ProductModel.table, id);
    print('products Raw $products');
    if (products.length > 0) {
      return ProductModel.fromMap(products[0]);
    }
    return null;
    // return products.map((item) => ProductModel.fromMap(item)).toList();
  }

  Future<List<CategoryModel>?> getCategories() async {
    await DB.init();
    List<Map<String, dynamic>> categories = await DB.query(CategoryModel.table);
    return categories.map((item) => CategoryModel.fromMap(item)).toList();
  }

  Future<bool> deleteProduct(ProductModel? model) async {
    await DB.init();
    bool isSaved = false;
    if (model != null) {
      int inserted = await DB.delete(ProductModel.table, model);
      print('Delete $inserted');
      isSaved = inserted == 1 ? true : false;
    }
    return isSaved;
  }
}
