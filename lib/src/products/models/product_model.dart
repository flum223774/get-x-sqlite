import 'package:get_x_sqlite/core/base/base_model.dart';

class ProductModel extends Model {
  static String table = 'products';

  int? id;
  String productName;
  int? categoryId;
  String productDesc;
  double? price;
  String? productPic;

  ProductModel({
    this.id,
    required this.productName,
    this.categoryId,
    required this.productDesc,
    required this.price,
    this.productPic,
  });

  static ProductModel init() {
    return ProductModel(
        productName: '', productDesc: '', price: 0);
  }

  ProductModel copyWith(
      {String? productName,
      int? categoryId,
      String? productDesc,
      double? price,
      String? productPic}) {
    return ProductModel(
      id: this.id,
      productName: productName ?? this.productName,
      categoryId: categoryId ?? this.categoryId,
      productDesc: productDesc ?? this.productDesc,
      price: price ?? this.price,
      productPic: productPic ?? this.productPic,
    );
  }

  static ProductModel fromMap(Map<String, dynamic> map) {
    return ProductModel(
      id: map["id"],
      productName: map['productName'].toString(),
      categoryId: map['categoryId'],
      productDesc: '${map['productDesc']}',
      price: map['price'],
      productPic: map['productPic'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': id,
      'productName': productName,
      'categoryId': categoryId,
      'productDesc': productDesc,
      'price': price,
      'productPic': productPic
    };

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }
}
