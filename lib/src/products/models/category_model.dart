import 'package:get_x_sqlite/core/base/base_model.dart';

class CategoryModel extends Model {
  static String table = 'product_categories';

  String categoryName;
  int categoryId;

  CategoryModel({
    required this.categoryId,
    required this.categoryName,
  });

  static CategoryModel fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      categoryId: map["id"],
      categoryName: map['categoryName'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'categoryName': categoryName,
    };

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }
}
