import 'package:get/get.dart';
import 'package:get_x_sqlite/src/products/bindings/product_binding.dart';
import 'package:get_x_sqlite/src/products/bindings/product_form_binding.dart';
import 'package:get_x_sqlite/src/products/views/add_product.dart';
import 'package:get_x_sqlite/src/products/views/product_view.dart';

class AppRoutes {
  AppRoutes._();

  static final routes = [
    GetPage(name: '/products', page: () => ProductView(), binding: ProductBinding(), children: [
      GetPage(
          name: '/product_form',
          binding: ProductFormBinding(),
          page: () => ProductForm(),
          children: [])
    ])
  ];
}
