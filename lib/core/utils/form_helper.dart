import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_x_sqlite/src/products/models/category_model.dart';
import 'package:image_picker/image_picker.dart';

class FormHelper {
  static Widget textInput(
    BuildContext context,
    Object? initialValue,
    Function onChanged, {
    bool isTextArea = false,
    bool isNumberInput = false,
    Function? onValidate,
    Widget? prefixIcon,
    Widget? suffixIcon,
  }) {
    return TextFormField(
      initialValue: initialValue != null ? initialValue.toString() : "",
      decoration: fieldDecoration(context, "", ""),
      maxLines: !isTextArea ? 1 : 3,
      keyboardType: isNumberInput ? TextInputType.number : TextInputType.text,
      onChanged: (String value) {
        return onChanged(value);
      },
      validator: (value) {
        if (onValidate != null) {
          return onValidate(value);
        }
      },
    );
  }

  static InputDecoration fieldDecoration(
    BuildContext context,
    String hintText,
    String helperText, {
    Widget? prefixIcon,
    Widget? suffixIcon,
  }) {
    return InputDecoration(
      contentPadding: EdgeInsets.all(6),
      hintText: hintText,
      helperText: helperText,
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(context).primaryColor,
          width: 1,
        ),
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(
          color: Theme.of(context).primaryColor,
          width: 1,
        ),
      ),
    );
  }

  static Widget selectDropdown(
    BuildContext context,
    Object? initialValue,
    dynamic data,
    Function onChanged, {
    Function? onValidate,
  }) {
    return Container(
      height: 75,
      padding: EdgeInsets.only(top: 5),
      child:  DropdownButtonFormField<String>(
        hint:  Text("Select"),
        value: initialValue != null ? initialValue.toString() : null,
        isDense: true,
        onChanged: (String? newValue) {
          FocusScope.of(context).requestFocus( FocusNode());
          onChanged(newValue);
        },
        validator: (value) {
          if (onValidate != null) {
            return onValidate(value);
          }
        },
        decoration: fieldDecoration(context, "", ""),
        items: data.map<DropdownMenuItem<String>>(
          (CategoryModel data) {
            return DropdownMenuItem<String>(
              value: data.categoryId.toString(),
              child:  Text(
                data.categoryName,
                style:  TextStyle(color: Colors.black),
              ),
            );
          },
        ).toList(),
      ),
    );
  }

  static Widget fieldLabel(String labelName) {
    return  Padding(
      padding: EdgeInsets.fromLTRB(0, 5, 0, 10),
      child: Text(
        labelName,
        style:  TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15.0,
        ),
      ),
    );
  }

  static Widget picPicker(String? fileName, Function onFilePicked) {
    Future<PickedFile?> _imageFile;
    ImagePicker _picker =  ImagePicker();

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 35.0,
              width: 35.0,
              child:  IconButton(
                padding: EdgeInsets.all(0),
                icon: Icon(Icons.image, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.gallery);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
            SizedBox(
              height: 35.0,
              width: 35.0,
              child:  IconButton(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                icon: Icon(Icons.camera, size: 35.0),
                onPressed: () {
                  _imageFile = _picker.getImage(source: ImageSource.camera);
                  _imageFile.then((file) async {
                    onFilePicked(file);
                  });
                },
              ),
            ),
          ],
        ),
        fileName != null
            ? Image.file(
                File(fileName),
                width: 300,
                height: 300,
              )
            :  Container()
      ],
    );
  }

  static void showMessage(BuildContext context, String title, String message,
      String buttonText, Function onPressed,
      {bool isConfirmationDialog = false,
      String buttonText2 = "",
      Function()? onPressed2}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title:  Text(title),
          content:  Text(message),
          actions: [
            ElevatedButton(
              onPressed: () => onPressed,
              child: Text(buttonText),
            ),
            Visibility(
              visible: isConfirmationDialog,
              child:  TextButton(
                onPressed: onPressed2,
                child:  Text(buttonText2),
              ),
            ),
          ],
        );
      },
    );
  }
}
