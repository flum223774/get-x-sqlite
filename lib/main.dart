import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_sqlite/configs/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/products',
      getPages: AppRoutes.routes,
    );
  }
}
